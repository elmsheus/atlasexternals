#
# Package building GDB as part of the offline software build.
#

# The name of the package:
atlas_subdir( Gdb )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Set up a dependency on Python:
find_package( PythonLibs )
find_package( ZLIB )

# Temporary build directory:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GdbBuild )

# Contents of the configuration script. Note that it's extremely difficult
# to use quotes of any kind inside of ExternalProject_Add, in conjunction
# with the way we log the build messages in the nightly. So just constructing
# a script that executes the configuration for us, is the most robust thing
# to do.
set( _cmd "sh -c \"./configure --prefix=${_buildDir}" )
set( _cmd "${_cmd} --with-system-gdbinit=${_buildDir}/etc/gdbinit" )
set( _cmd "${_cmd} --with-python=${PYTHON_ROOT} --enable-install-libiberty" )
set( _cmd "${_cmd} LDFLAGS=-L${PYTHON_ROOT}/lib" )
set( _cmd "${_cmd} CFLAGS=\\\"-fPIC -Wno-implicit-function-declaration\\\"" )
set( _cmd "${_cmd} CXXFLAGS=-fPIC\"" )
file( GENERATE
   OUTPUT ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   CONTENT ${_cmd} )

# Libraries to link against:
set( _libraries
  ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${CMAKE_STATIC_LIBRARY_PREFIX}bfd${CMAKE_STATIC_LIBRARY_SUFFIX} )
atlas_os_id( osName osIsValid )
if( osIsValid AND ( ( "${osName}" STREQUAL "slc6" ) OR
         ( "${osName}" STREQUAL "centos7" ) ) )
  list( APPEND _libraries
    ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/lib64/${CMAKE_STATIC_LIBRARY_PREFIX}iberty${CMAKE_STATIC_LIBRARY_SUFFIX} )
else()
  list( APPEND _libraries
    ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${CMAKE_STATIC_LIBRARY_PREFIX}iberty${CMAKE_STATIC_LIBRARY_SUFFIX} )
endif()

# Set up the build of GDB for the build area:
ExternalProject_Add( Gdb
   PREFIX ${CMAKE_BINARY_DIR}
   URL http://ftp.gnu.org/gnu/gdb/gdb-7.11.tar.gz
   URL_MD5 f585059252836a981ea5db9a5f8ce97f
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND
   sh ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   BUILD_COMMAND make
   COMMAND make install
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR>
   BUILD_BYPRODUCTS ${_libraries}
   )
add_dependencies( Package_Gdb Gdb )

# Set up its installation:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Build the package's application:
atlas_add_executable( resolveAtlasAddr2Line src/resolveAtlasAddr2Line.cxx
   INCLUDE_DIRS ${CMAKE_INCLUDE_OUTPUT_DIRECTORY} ${ZLIB_INCLUDE_DIRS}
   LINK_LIBRARIES ${_libraries} ${ZLIB_LIBRARIES} )
add_dependencies( resolveAtlasAddr2Line Gdb )

# Install its other resources:
atlas_install_python_modules( python/__init__.py python/gdbhacks )
atlas_install_scripts( scripts/atlasAddress2Line )
