Boost C++ Libraries
===================

This package is used to build Boost for the standalone analysis release.

If an appropriate version of Boost is already found on the system, then the
package doesn't do anything. Unless you force it to build Boost no matter what,
using the

    -DBOOST_FORCE_BUILD:BOOL=TRUE

CMake configuration option.
