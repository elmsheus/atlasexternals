Python
======

This package is used to build Python for the offline / analysis release.

While most platforms provide Python natively, the ATLAS software strictly
needs Python 2.7.X to work. Which is not the system default on SLC6 most
notably. So we need to build our own version.

Since other externals may depend on Python for their own build, it's not
possible to force the build of Python on a system that already provides
an appropriate version. As handling the logic of this correctly in the
configuration of the dependent packages would be uncomfortably complicated.
So the logic is simply that if Python 2.7 is found during the CMake
configuration, then this package doesn't do anything. If it's not found,
then the package builds it.
