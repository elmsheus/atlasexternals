HepMCAnalysis Tool
==================

This package builds the HepMCAnalysis code from sources taken from the LCG
webpage itself.

The custom build of this package is only necessary because it uses CLHEP, and
we use a custom version of CLHEP and not what LCG provides.

To build this package successfully, it must be built together with the FastJet
and CLHEP packages.
