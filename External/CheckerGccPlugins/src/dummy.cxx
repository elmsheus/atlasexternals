// $Id$
/**
 * @file CheckerGccPlugins/src/dummy.cxx
 * @author scott snyder <snyder@bnl.gov>
 * @date Feb, 2016
 * @brief Empty source file to prevent errors in clang builds.
 */
