#
# CMake configuration file building the BLAS library as part of the project
# build.
#

# The name of the package:
atlas_subdir( Blas )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Extend the CMAKE_Fortran_FLAGS variable with the build type specific
# flags, as we only use that variable in setting up the make.inc file.
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   string( TOUPPER ${CMAKE_BUILD_TYPE} _type )
   set( CMAKE_Fortran_FLAGS
      "${CMAKE_Fortran_FLAGS} ${CMAKE_Fortran_FLAGS_${_type}}" )
   unset( _type )
endif()

# Configure BLAS's makefile:
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/src/make.inc.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/make.inc @ONLY )

# Build BLAS for the build area:
ExternalProject_Add( Blas
   PREFIX ${CMAKE_BINARY_DIR}
   URL ${CMAKE_CURRENT_SOURCE_DIR}/src/blas-20070405.tar.gz
   URL_MD5 64428bb36a41ab470039354eeaaa1472
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E copy
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/make.inc make.inc
   BUILD_IN_SOURCE 1
   BUILD_COMMAND make
   INSTALL_COMMAND ${CMAKE_COMMAND} -E make_directory
   ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}
   COMMAND ${CMAKE_COMMAND} -E copy libblas.a
   ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/
   )
add_dependencies( Package_Blas Blas )

# Install BLAS:
install( FILES ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/libblas.a
   DESTINATION ${CMAKE_INSTALL_LIBDIR} OPTIONAL )
