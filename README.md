ATLAS Externals Repository
==========================

This repository holds the code for building all flavours of ATLAS
"externals" projects. Projects holding code not developed by ATLAS,
but used by the offline/simulation/analysis software of the
experiment.

Code In The Repository
----------------------

The main project configurations can be found in the `Projects` directory.
You need to point CMake to one of those folders to build one of the
specific externals projects.

Other directories hold packages that either build, or set up some external
software to be used by downstream projects.

Build / Install
---------------

The projects can be built in a fairly standard way. The `AnalysisBaseExternals`
project should be possible to build on practically any platform that
supports C++11. All the other projects on the other hand need to be built
on some platform for which an LCG release is available.

You are highly recommended to use an out-of-source build for all projects,
even though in-source builds may also work. But they are generally not
supported/tested.

The most common build procedure is:

    mkdir build
    cd build/
    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCTEST_USE_LAUNCHERS=TRUE ../atlasexternals/Projects/AthenaExternals/
    make
    make install DESTDIR=/some/location
