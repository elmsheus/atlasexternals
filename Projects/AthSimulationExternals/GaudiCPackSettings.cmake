#
# This file is picked up by Gaudi, when it's built against
# AthSimulationExternals. It configures how Gaudi should use CPack.
#

# Tell the user what's happening:
message( STATUS "Configuring GAUDI to use the AthSimulationExternals CPack settings" )

# Remember the project name:
set( _projectName ${CMAKE_PROJECT_NAME} )
# Remember the project version:
set( _projectVersion ${CMAKE_PROJECT_VERSION} )

# Set the GAUDI version according to NICOS, if the environment variables
# are available:
if( NOT "$ENV{NICOS_ATLAS_ALT_RELEASE}" STREQUAL "" AND
      NOT "$ENV{NICOS_ATLAS_ALT_RELEASE}" STREQUAL "None" )
   set( CMAKE_PROJECT_VERSION $ENV{NICOS_ATLAS_ALT_RELEASE} )
elseif( NOT "$ENV{NICOS_ATLAS_RELEASE}" STREQUAL "" )
   set( CMAKE_PROJECT_VERSION $ENV{NICOS_ATLAS_RELEASE} )
else()
   set( CMAKE_PROJECT_VERSION ${AthSimulationExternals_VERSION} )
endif()

# Call the package GAUDI, and not Gaudi:
set( CMAKE_PROJECT_NAME "GAUDI" )

# Make the GAUDI package depend on the AthSimulationExternals one:
set( ATLAS_BASE_PROJECTS AthSimulationExternals
   ${AthSimulationExternals_VERSION} )

# Do the regular ATLAS CPack configuration:
include( AtlasInternals )
atlas_cpack_setup()

# Clean up:
set( CMAKE_PROJECT_VERSION ${_projectVersion} )
unset( _projectVersion )
set( CMAKE_PROJECT_NAME ${_projectName} )
unset( _projectName )
unset( ATLAS_BASE_PROJECTS )
