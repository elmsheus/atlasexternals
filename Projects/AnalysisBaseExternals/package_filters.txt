#
# List of packages to build as part of AnalysisBaseExternals.
#
+ External/Boost
+ External/Eigen
+ External/FastJet
+ External/FastJetContrib
+ External/GoogleTest
+ External/HEPUtils
+ External/Lhapdf
+ External/MCUtils
+ External/Python
+ External/ROOT
- .*
