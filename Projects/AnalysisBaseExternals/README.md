AnalysisBaseExternals
=====================

This project builds all the externals needed by the standalone AnalysisBase
analysis release of ATLAS.

Many externals for AnalysisBase can be taken from the build system itself.
The project will not force building all of its externals by default if an
appropriate version of that external is already available on the build
machine. For instance for ROOT or Boost.
