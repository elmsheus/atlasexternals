#
# List of packages to build as part of AthenaExternals.
#
+ External/APE
+ External/Blas
+ External/CLHEP
+ External/FFTW
+ External/FastJet
+ External/FastJetContrib
+ External/GPerfTools
+ External/Gdb
+ External/Geant4
+ External/GoogleTest
+ External/HepMCAnalysis
+ External/Lapack
+ External/MKL
+ External/Simage
+ External/dSFMT
+ External/yampl
+ External/CheckerGccPlugins
- .*
